<?php
#Gets the IP from the browser remote address
$IP = $_SERVER['REMOTE_ADDR'];
$msg = "Incoming IP from " . $IP;

#Wraps into a javascript function to send to console, however heroku is weird in that console logs dont show up because
#the application default is set to php not js. So its not expecting any js calls.
echo "<script> console.log('{$msg}') </script>";

#This header location function essentially acts as a redirect. The code above runs within micro seconds, and the
#user is redirected back to index.html or whatever page you want to send them to.
header("Location: ../index.html");
?>

