<?php
#This type of page the user will never physically see, its simply run to get information and then
#sends the user to another page.

#Sets up that we are in a session. We will be setting up session variables for authentication
session_start();

if ($_POST['username'] =="user" && $_POST['password'] == "pass"){
    #if the username from the login form is 'user' and if the password from the login form is 'pass' then;
    #set the session username variable to the username the user has entered
    $_SESSION['username'] = $_POST['username'];
    #redirect to admin page
    header("Location: admin.php");
} else{
    #if the username or password is incorrect, go back to the login page.
    header("Location: login.html");
};