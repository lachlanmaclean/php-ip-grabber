<?php

#Session start is one session. Single tab. Session closes when tab is closed or browser is closed.
#Think of it like a cookie

#Sessions are great because they can be used for authentication as well as global variables that can be
#accessed on other pages without the need of explicitly sending it and catching for a message.

session_start();

#Checks if the session is already open with a username variable.
if ( isset( $_SESSION['username'] ) ) {
} else { #if no username variable is found in the browser session, redirect to login
    header("Location: ../index.html");
} ?>
<!-- This code above can be copy pasted into any 'protected' page you want. -->

<h1>Admin Console</h1>
<?php #reads the file line by line, recursive.
$file = fopen("../resources/file.txt","r");

while(! feof($file)) #while the 'end of file' has not been reached keep doing this..
{
    echo fgets($file). "<br />"; #echo line by line, until the end of file is readhed
}

fclose($file); #sets the end of file point, as well as ending the process of reading
?>


<p><!-- This is just a log out button-->
<a href="logout.php">Logout</a>
