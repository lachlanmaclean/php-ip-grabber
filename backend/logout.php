<?php
#This ensures we are in our session, so we can kill it.
#PHP can have multiple sessions, ie multiple tabs. Not very useful but it happens
session_start();

#This closes the current active session
session_destroy();

#redirects back to the login screen
header("Location: login.html");
?>